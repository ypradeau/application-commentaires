<?php
///////// STRUCTURE BOUCLE
/// Instruction (Un truc qui se passe)
///////// STRUCTURE BOUCLE


// Boucle FOR - Pour
// Pour 'i' allant de 0 à 5
    // Implicite : Chaque tour de bloucle ajoute +1 à i
// Fin Pour


// --> On entre dans la boucle
// Tour 1 -> i = 0
// Tour 2 -> i = 1
// Tour 3 -> i = 2
// Tour 4 -> i = 3
// Tour 5 -> i = 4
// Tour 6 -> i = 5
// --> Sortie de la boucle


// Le but : De faire passer 2010 jusqu'à 2020
// Pour 'i' allant de 2010 à 2020
    // On affiche l'année
// Fin Pour

// --> On entre dans la boucle
// Tour 1 -> i = 2010 -> Affichage : 2010
// Tour 2 -> i = 2011 -> Affichage : 2011
// Tour 3 -> i = 2010 -> Affichage : 2012
// Tour 4 -> i = 2010 -> Affichage : 2013
// Tour 5 -> i = 2010 -> Affichage : 2014
// Tour 6 -> i = 2010 -> Affichage : 2015
// Tour 7 -> i = 2010 -> Affichage : 2016
// Tour 8 -> i = 2010 -> Affichage : 2017
// Tour 9 -> i = 2010 -> Affichage : 2018
// Tour 10 -> i = 2010 -> Affichage : 2019
// Tour 11 -> i = 2010 -> Affichage : 2020
// --> Sortie de la boucle


$annee = 2010;

// for(initialisation; condition; incrementation/decrementation)
for($compteur = 0; $compteur <= 10; $compteur++) {
    echo "Compteur est égal à " . $compteur . " <br />";
}

for($compteur = 10; $compteur > 0; $compteur--) {
    echo "Compteur est égal à " . $compteur . " <br />";
}

$prenoms = array(
    "Michel",
    "Micheline",
    "Tibo",
    "Vanessa"
);

echo $prenoms[0];
echo $prenoms[1];
echo $prenoms[2];
echo $prenoms[3];
echo "<br />";

for($index = 0; $index <= 3; $index++) {
    echo $prenoms[$index] . "<br />";
    // Au tour 1 -> $index = 0; donc on echo $prenoms[0]
    // Au tour 2 -> $index = 1; donc on echo $prenoms[1]
    // Au tour 3 -> $index = 2; donc on echo $prenoms[2]
    // Au tour 4 -> $index = 3; donc on echo $prenoms[3]
}

// 0 - 2 - 4 - 6 - 8 - 10 - 12 - 14 - 16 - 18 - 20
for($pair = 0; $pair <= 20; $pair+=2) {
    echo $pair . "<br />";
}

// Afficher tous les index impairs du tableau : Micheline - Vanessa
for($prenoms_impairs = 1; $prenoms_impairs <= 3; $prenoms_impairs+=2) {
    echo $prenoms[$prenoms_impairs] . "<br />";
}

// Afficher tous les index impairs du tableau mais on ajoutant 1 à chaque tour
for($prenoms_impairs = 0; $prenoms_impairs <= 3; $prenoms_impairs++) {
    if($prenoms_impairs != 0 && $prenoms_impairs != 2) {
        echo $prenoms[$prenoms_impairs] . "<br />";
    }
}
// Afficher tous les index impairs du tableau mais on ajoutant 1 à chaque tour
for($prenoms_impairs = 0; $prenoms_impairs <= 3; $prenoms_impairs++) {
    if($prenoms_impairs % 2 != 0) {
        echo $prenoms[$prenoms_impairs] . "<br />";
    }
}


$chiffres = array();

// A chaque tour de boucle, il ajoute de 0 à 9 un chiffre dans une entrée de $chiffres
for($index = 0; $index <= 9; $index++) {
    $chiffres[] = $index;
}
var_dump($chiffres);