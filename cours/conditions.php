<?php

$persons = array(
    array(
        "prenom" => "Michel",
        "age"    => 65,
        "permis" => true,
        "enfants"=> 2
    ),

    array(
        "prenom" => "Micheline",
        "age"    => 75,
        "permis" => false,
        "enfants"=> 3
    ),

    array(
        "prenom" => "Tibo",
        "age"    => 16,
        "permis" => false,
        "enfants"=> 0
    ),

    array(
        "prenom" => "Vanessa",
        "age"    => 28,
        "permis" => true,
        "enfants"=> 1
    )
);

var_dump($persons);

// $persons[1]["permis"]
if($persons[1]["permis"] == true) {
    echo "Micheline, elle a son permis<br />";
} else {
    echo "Micheline, elle a pas son permis<br />";
}

if($persons[2]["age"] >= 18) {
    echo "Damn les gens, Tibo est majeur <br />";
} else {
    echo "Tibo lé petit <br />";
}

// Si Vanessa est majeure
// Sinon si Michel n'a pas son permis
// Sinon ..
if($persons[3]["age"] >= 18) {
    echo "Vanessa est majeure <br />";
} elseif($persons[0]["permis"] == false) {
    echo "Michel n'a pas son permis <br />";
} else {
    echo "...";
}

// Si le prénom de Tibo est Tibo
// Si le prénom de Vanessa est Vanéssa
if($persons[2]["prenom"] == "Tibo") {
    echo "Tibo s'appelle bien Tibo <br />";
}
if($persons[3]["prenom"] == "Vanéssa") {
    echo "Vanessa a un accent dans son prénom <br />";
}
if($persons[3]["prenom"] != "Vanéssa") {
    echo "Vanessa n'a pas d'accent dans son prénom <br />";
}


switch($persons[1]["enfants"]) {
    case 3:
        echo "Micheline a 3 enfants <br />";
        break;
    case 2:
        echo "Micheline a 2 enfants <br />";
        break;
    case 1:
        echo "Micheline a 1 enfant <br />";
        break;
    default: // si aucune valeur au-dessus correspond
        echo "Micheline a un nombre indéterminé d'enfants voire aucun <br />";
        break;
}

switch($persons[1]["enfants"]) {
    case 3:
        echo "Passé par 3";
    case 2:
        echo "Passé par 2";
    case 1:
        echo "Micheline a 1 ou 3 enfant(s)";
        break;
    default:
        break;
}