<?php

// Boucle WHILE - Tant que
$compteur = 0;

while($compteur <= 5) {
    echo "Le compteur est égal à " . $compteur . "<br />";
    $compteur++;
}


// Boucle DO ... WHILE - Faire ... Tant que
$age = 0;
do {
    echo "J'ai " . $age . " an(s)<br />";
    $age++;
} while($age <= 18);