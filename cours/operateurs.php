<?php

    $a = 15;
    $b = 'Chocolat';
    $c = array(true, 2.5, "chaud");
    $d = 0;

    // Opérateurs arithmétiques
    // DOCUMENTATION : https://www.php.net/manual/fr/language.operators.arithmetic.php
    echo ($a + $c[1]) . "<br />";
    echo ($a + $b) . "<br />";
    echo ($a - $c[1]) . "<br />";
    echo ($a / $c[1]) . "<br />";
    echo ($a * $c[1]) . "<br />";

    // La concaténation
    // Documentation : https://www.php.net/manual/fr/language.operators.string.php
    // Colle "chocolat" et "chaud"
    echo $b . $c[2] . "<br />";
    // Colle "chocolat" et " " et "chaud"
    echo $b . " " . $c[2] . "<br />";
    // Colle "chocolat" et " froid"
    echo $b . " froid<br />";
    // Colle 15 et "chocolat"
    echo $a . $b . "<br />";
    // Colle 15 et " " et "chocolat" et "s"
    echo $a . " " . $b . "s<br />";

    // Priorités de calcul
    echo $a + ($c[1] * $d) . "<br />";
    echo ($a + $c[1]) * $d . "<br />";

    // Opérateur Modulo
    echo 2 / 1 . "<br />";
    echo 2 % 1 . "<br />";
    echo 1 % 2;

    // Opérateurs d'incrémentation et de décrémentation
    // DOCUMENTATION : https://www.php.net/manual/fr/language.operators.increment.php
    $compteur = 10;

    $compteur++;
    // $compteur = $compteur + 1;
    var_dump($compteur);

    $compteur--;
    // $compteur = $compteur - 1;
    var_dump($compteur);

    // Opérateurs d'affectation
    // DOCUMENTATION : https://www.php.net/manual/fr/language.operators.assignment.php
    $compteur += 2;
    // $compteur = $compteur + 2;
    var_dump($compteur);

    $compteur -= 2;
    // $compteur = $compteur - 2;
    var_dump($compteur);

    $compteur *= 2;
    // $compteur = $compteur * 2;
    var_dump($compteur);

    $compteur /= 2;
    // $compteur = $compteur / 2;
    var_dump($compteur);


    $text = "Je m'appelle";
    //$text = $text . " Yohan";
    $text .= " Yohan";
    echo $text;