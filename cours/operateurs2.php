<?php

$condition = false;
$age = 16;
$derogation = true;

// Structure conditionelle
if($condition) {
    echo "Je suis vrai";
} else {
    echo "Je suis faux";
}

// Opérateurs de comparaison
$a = 1;
$b = "1";
$c = 5;
$d = true;
$e = false;
$f = 0;

// Opérateur "==" -> compare la valeur des deux variables sans prendre en compte leur type
var_dump($a == $b);

// Opérateur "===" -> compare la valeur et le type des deux variables
var_dump($a === $b);

// Opérateur ">" permet de s'assurer que la variable a est bien strictement plus grande que la variable b
var_dump($a > $c);
var_dump($a > 1);

// Opérateur "<" permet de s'assurer que la variable a est bien strictement plus petit que la variable b
var_dump($a < $c);
var_dump($a < 1);

// Opérateur ">=" permet de s'assurer que la variable a est bien plus grande ou égale à la variable b
var_dump($a >= $c);
var_dump($a >= 1);

// Opérateur "<=" permet de s'assurer que la variable a est bien plus petite ou égale à la variable b
var_dump($a <= $c);
var_dump($a <= 1);

// Opérateur "!=" -> compare la différence de valeur des deux variables sans prendre en compte leur type
var_dump($a != $b);

// Opérateur "!==" -> compare la différence de valeur et de type des deux variables
var_dump($a !== $b);

var_dump($d);
// Opérateur "!" permet d'avoir le contraire de ...
var_dump(!$d);

// Comparaison entre 0 et false
var_dump($f == $e);

// Comparaison entre 1 et true
var_dump($a == $d);

// Comparaison type et valeur entre 0 et false / 1 et true
var_dump($f === $e);
var_dump($a === $d);