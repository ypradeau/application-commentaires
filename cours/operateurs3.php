<?php

$persons = array(
    array(
        "prenom" => "Michel",
        "age"    => 65,
        "permis" => true,
        "enfants"=> 2,
    ),

    array(
        "prenom" => "Micheline",
        "age"    => 75,
        "permis" => false,
        "enfants"=> 3
    ),

    array(
        "prenom" => "Tibo",
        "age"    => 16,
        "permis" => false,
        "enfants"=> 0
    ),

    array(
        "prenom" => "Vanessa",
        "age"    => 28,
        "permis" => true,
        "enfants"=> 1
    )
);

// Opérateurs de logique
// DOCUMENTATION : https://www.php.net/manual/fr/language.operators.logical.php

// OPERATEURS
// && Permet de dire ET
// || Permet de dire OU

// COMBINAISONS
/*
 * TRUE && TRUE -> TRUE
 * TRUE && FALSE -> FALSE
 * FALSE && TRUE -> FALSE
 * FALSE && FALSE -> FALSE
 */

// Michel a son permis ET Micheline n'a pas son permis
// Michel a bien son permis --> TRUE
// Micheline n'a pas son permis --> TRUE
var_dump($persons[0]["permis"] == true && $persons[1]["permis"] != true);

// Michel a son permis ET Micheline l'a aussi
// Michel a bien son permis --> TRUE
// Micheline n'a pas son permis --> FALSE
var_dump($persons[0]["permis"] == true && $persons[1]["permis"] == true);

// Michel n'a pas son permis ET Micheline a son permis
// Michel a son permis -> FALSE
// Micheline n'a pas son permis -> FALSE
var_dump($persons[0]["permis"] != true && $persons[1]["permis"] != false);


// COMBINAISONS
/*
 * TRUE || TRUE -> TRUE
 * TRUE || FALSE -> TRUE
 * FALSE || TRUE -> TRUE
 * FALSE || FALSE -> FALSE
 */

// Vanessa est majeure OU Tibo est mineur
// Vanessa est majeure -> TRUE
// Tibo est mineur -> TRUE
var_dump($persons[3]["age"] >= 18 || $persons[2]["age"] < 18);

// Vanessa a son permis OU Tibo a des enfants
// Vanessa a son permis -> TRUE
// Tibo n'a pas d'enfants -> FALSE
var_dump($persons[3]["permis"] != false || $persons[2]["enfants"] >= 1);

// Vanessa a plus de 40 ans OU Tibo a son permis
// Vanessa n'a pas plus de 40ans -> FALSE
// Tibo n'a pas son permis -> FALSE
var_dump($persons[3]["age"] > 40 || $persons[2]["permis"] == true);

// Autre écriture
// && -> AND
var_dump($persons[0]["permis"] != true AND $persons[1]["permis"] != false);
// || -> OR
var_dump($persons[3]["age"] > 40 OR $persons[2]["permis"] == true);