<?php

$persons = array(
    array(
        "prenom" => "Michel",
        "age"    => 65,
        "permis" => true,
        "enfants"=> 2,
        "sexe" => "masculin"
    ),

    array(
        "prenom" => "Micheline",
        "age"    => 75,
        "permis" => false,
        "enfants"=> 3,
        "sexe" => "féminin"
    ),

    array(
        "prenom" => "Tibo",
        "age"    => 16,
        "permis" => false,
        "enfants"=> 0,
        "sexe" => "masculin"
    ),

    array(
        "prenom" => "Vanessa",
        "age"    => 28,
        "permis" => true,
        "enfants"=> 1,
        "sexe" => "féminin"
    )
);

// for(initialisation; condition; incrémentation/décrémentation) 


// 1_ Afficher tous les prénoms des personnes dans le tableau 
for($prenoms = 0; $prenoms <= 3; $prenoms++) {
    echo $persons[$prenoms]["prenom"] . ", ";
}

echo "<br /><br />";

// 2_ Afficher l'âge de Michel et de Vanessa (sans condition - jouer avec l'incrémentation)
for($age = 0; $age <= 3; $age+=3) {
    echo $persons[$age]["prenom"] . " a " . $persons[$age]["age"] . " ans.<br />";
}

echo "<br /><br />";

// 3_ Afficher le prénom de ceux qui ont un âge pair (conditions) 
for($agepair = 0; $agepair <= 3; $agepair++) {
    if ($persons[$agepair]["age"] % 2 == 0) {
        echo $persons[$agepair]["prenom"] . " a un âge pair. <br />";
    }
}

echo "<br /><br />";

// 4_ Afficher le prénom, l'âge et le nombre d'enfants des mamans (conditions)
for($mamans = 0; $mamans <= 3; $mamans++) {
    if($persons[$mamans]["enfants"] > 0 && $persons[$mamans]["sexe"] == "féminin") {
        echo $persons[$mamans]["prenom"] . " a " . $persons[$mamans]["age"] . " ans et " . $persons[$mamans]["enfants"] . " enfant(s). <br />";
    }
}





































?>