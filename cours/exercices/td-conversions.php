<?php

$consigne = array(
    1579334742,
    "heure(s)",
    "minute(s)",
    "seconde(s)"
);

// 1. Transformer 5625 en heures / minutes / secondes
$total = $consigne[0];

$hours = intval($total / 3600);
$total = $total - ($hours*3600);

$minutes = intval($total / 60);
$total = $total - ($minutes * 60);

$secondes = $total;

// 2. Stocker dans un nouveau tableau, les heures, les minutes et les secondes
$results = [
    $hours,
    $minutes,
    $secondes
];

// 3. Afficher le résultats suivant : Cela fait X heure(s) X minute(s) X seconde(s)
echo "Cela fait " . $results[0] . ' ' . $consigne[1] . ', ' . $results[1] . ' ' . $consigne[2] . ' et ' . $results[2] . ' ' . $consigne[3] . '<br />';

$total = $consigne[0];

$secondes = $total % 60;
$total = $total - $secondes;

$minutes = ($total % 3600) / 60;
$total = $total - ($total % 3600);

$hours = $total / 3600;

$results = [
    $hours,
    $minutes,
    $secondes
];

echo "Cela fait " . $results[0] . ' ' . $consigne[1] . ', ' . $results[1] . ' ' . $consigne[2] . ' et ' . $results[2] . ' ' . $consigne[3];
