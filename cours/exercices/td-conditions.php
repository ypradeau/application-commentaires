<?php

$persons = array(
    array(
        "prenom" => "Michel",
        "age"    => 65,
        "permis" => true,
        "enfants"=> 2,
        "sexe" => "masculin"
    ),

    array(
        "prenom" => "Micheline",
        "age"    => 75,
        "permis" => false,
        "enfants"=> 3,
        "sexe" => "feminin"
    ),

    array(
        "prenom" => "Tibo",
        "age"    => 16,
        "permis" => false,
        "enfants"=> 0,
        "sexe" => "masculin"
    ),

    array(
        "prenom" => "Vanessa",
        "age"    => 28,
        "permis" => true,
        "enfants"=> 1,
        "sexe" => "feminin"
    )
);


// Si Vanessa et Micheline sont des filles
    // On affiche que Vanessa et micheline sont des filles
// Sinon
    // On affiche que y'a un trans dans les parages
if($persons[3]["sexe"] == "feminin" && $persons[1]["sexe"] == "feminin") {
    echo "Vanessa et Micheline sont des filles <br />"; // ICI
} else {
    echo "Y'a un trans dans les parages <br />";
}

// Si Vanessa OU Tibo OU Michel ont leur permis
    // On affiche Vanessa ou Tibo ou Michel ont leur permis
if($persons[3]["permis"] == true || $persons[2]["permis"] == true || $persons[0]["permis"] == true) {
    echo "Vanessa ou Tibo ou Michel ont leur permis <br />";  // ICI
}

// Si Micheline et Tibo ont leur permis
    // On affiche cachez-vous dans la rue
// Sinon si Michel a son permis OU Tibo est majeur OU Vanessa est un homme
    // On affiche Bienvenue à la maison
// Sinon
    // On affiche "Pas"
if($persons[1]["permis"] == true && $persons[2]["permis"] == true) {
    echo "Cachez-vous dans la rue <br />";
} elseif($persons[0]["permis"] == true || $persons[2]["age"] >= 18 || $persons[3]["sexe"] == "masculin") {
    echo "Bienvenue à la maison <br />"; // ICI
} else {
    echo "Pas <br />";
}

// Si Vanessa a un âge supérieur à 20ans et qu'elle est maman
    // On affiche que Vanessa est une femme mature
if($persons[3]["age"] > 20 && ($persons[3]["enfants"] > 0 && $persons[3]["sexe"] == "feminin")) {
    echo "Vanessa est une femme mature <br />"; // ICI
}

// Si Tibo a 16 ans et qu'il n'a pas son permis OU que Michel a son permis et qu'il a des enfants
    // On affiche que oui la condition compliquée elle est true
if(($persons[2]["age"] == 16 && $persons[2]["permis"] != true) || ($persons[0]["permis"] == true && $persons[0]["enfants"] > 0)) {
    echo "Oui la condition compliquée elle est true <br />";  // ICI
}

// Si Micheline a 4enfants ou qu'elle n'a pas son permis ET que Michel a comme prénom Michel ou qu'il a plus de 50ans ET que Tibo a des enfants
    // On affiche que c'te condition de mort elle passe
// Sinon Si Micheline est une femme ET que Vanessa est une femme
    // On affiche que Micheline et Vanessa ne sont pas les trans
// (FALSE || TRUE) && (TRUE || TRUE) && FALSE
// TRUE && TRUE && FALSE
// FALSE
if(($persons[1]["enfants"] == 4 || $persons[1]["permis"] != true) &&
    ($persons[0]["prenom"] == "Michel" || $persons[0]["age"] > 50) &&
    $persons[2]["enfants"] > 0) {
    echo "C'te condition de mort elle passe <br />";
} elseif ($persons[1]["sexe"] == "feminin" && $persons[3]["sexe"] == "feminin") {
    echo "Micheline et Vanessa ne sont pas les trans <br />";  // ICI
}