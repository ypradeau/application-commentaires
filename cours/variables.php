<?php

// DOCUMENTATION : https://www.php.net/manual/fr/language.types.intro.php

$chiffre = 0; // Variable de type int (Integer -> nombre entier)
$prenom = "Yohan"; // Variable de type string (Chaîne de caractères)
$prix = 10.2; // Variable de type float (Nombre flottant ou à virgule)
$majeur = true; // Variable de type booléen (Soit vrai, soit faux)

// Variable de type "Tableau"
$frigo = [];
$frigo = array();

// Première façon de remplir un tableau (Quand tu sais ce que tu veux mettre dedans)
$frigo = ["Pommes", 3, "Cerises", 10, "Lait"];

// Un index de tableau c'est ce qu'on appelle la clé (anglais: key)
echo '<pre>';
var_dump($frigo);
echo '</pre>';

echo $frigo[2];

// Créer un tableau sans savoir à l'avance combien de données on aura
$trousse = array();
$trousse[] = "Stylo";
$trousse[] = "Gomme";
$trousse[] = "Colle";

// Créer une entrée dans mon tableau en lui précisant l'index que je veux
$trousse[5] = "Ciseaux";

var_dump($trousse);

// Tableaux index "string"

$person = array(
  "prenom" => "Yohan",
    3 => "Age",
);
$person["sexe"] = "Masculin";

echo "Je suis de sexe " . $person["sexe"];

var_dump($person);