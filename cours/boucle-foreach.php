<?php

$persons = array(
    array(
        "prenom" => "Michel",
        "age"    => 65,
        "permis" => true,
        "enfants"=> 2,
        "sexe" => "masculin"
    ),

    array(
        "prenom" => "Micheline",
        "age"    => 75,
        "permis" => false,
        "enfants"=> 3,
        "sexe" => "feminin"
    ),

    array(
        "prenom" => "Tibo",
        "age"    => 16,
        "permis" => false,
        "enfants"=> 0,
        "sexe" => "masculin"
    ),

    array(
        "prenom" => "Vanessa",
        "age"    => 28,
        "permis" => true,
        "enfants"=> 1,
        "sexe" => "feminin"
    )
);

/*var_dump($persons);

// Uniquement utilisable sur les tableaux
foreach($persons as $person) {
    var_dump($person);
}

$prenoms = array("Michel", "Micheline", "Tibo", "Vanessa", "Celestin");

foreach($prenoms as $index => $prenom) {
    var_dump($index);
    var_dump($prenom);
} */

foreach($persons[0] as $index => $value) {
    var_dump($index);
    echo "<br />";
    var_dump($value);
    echo "<br />";
}

$posts = array(
    array(
        "id" => 1,
        "id_admin" => 1,
        "message" => "Ceci est un post",
        "created_at" => "30/04/2020"
    ),
    array(
        "id" => 2,
        "id_admin" => 1,
        "message" => "Ceci est un autre post",
        "created_at" => "01/05/2020"
    )
);

$admins = array(
    array(
        "id" => 1,
        "pseudo" => "Tictac"
    ),
    array(
        "id" => 2,
        "pseudo" => "Halloween"
    )
);

// ID DU POST - DATE DU POST : MESSAGE
foreach($posts as $index => $post) {
    echo $post["id"] . " - " . $post["created_at"] . " : " . $post["message"];
    echo "<br />";
}